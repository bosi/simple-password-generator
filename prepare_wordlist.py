import re
from random import shuffle

# source_file = "deu_newscrawl_2017_1M-words.txt"
source_file = "deu_mixed-typical_2011_1M-words.txt"
pw_wordlist_file = "pw_words.txt"
word_regex = "^[a-zA-ZöäüÖÄÜß]{4,}$"

words = []

with open(source_file) as file:
    for line in file:
        data = line.strip().split("\t")

        pattern = re.compile(word_regex)

        if pattern.match(data[1]):
            words.append(data[1])

shuffle(words)

with open(pw_wordlist_file, "w") as words_file:
    for word in words:
        words_file.write(word + "\n")
